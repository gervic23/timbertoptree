<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '');

/** MySQL database username */
define('DB_USER', '');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-xPj=YS @fQ8s_C!$>Xi6kQ6O|(_ulJ^lJox}/FjaYgXvq6r#>ov>zz4F|LIC&f?');
define('SECURE_AUTH_KEY',  'GS_G(xr=6nvS_K#Mt,,`,5r=0JAj2O4Ex$1 B$!NqV9?Ek&j(u>2Q#zctGQf ec>');
define('LOGGED_IN_KEY',    'fM4lX8nwUYvars4I7IXPxB`LlE`< I5Bm&IxS,Pu53[rRs6W}K!),a>Af#7u;5Tk');
define('NONCE_KEY',        'THV}83AK[[cuW$=,WG?Mm_1<DORS<6G{c:=aB:d$woQ{bU9Nn0i[5I$g@~5elT:x');
define('AUTH_SALT',        'hG2=#vwwJ:1Olcwz|>&6E=,59L8K1F ~5_Dg[4B|VxvmwZ=LWcQ{c6!!#H]+Pi>)');
define('SECURE_AUTH_SALT', 'RBi=l]cm6A2j~YuXB:H;QQ# ]9-isggc;a@UR<9=9Sv4^4t|!?+Y{qdK40G~cW>u');
define('LOGGED_IN_SALT',   'm}Hate=T&:VSM(}h9DYP0R;>)?)Rg:M:{QPa~!<1QU#xzcS5#C%FQ5s#-cY;,4[Z');
define('NONCE_SALT',       'yI^4/Rg.f>7Vccd|~Zpwru,%Qr&5u.q=eq#c:E d1]I`rncx: (Xf:g6IeVHv`0H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
