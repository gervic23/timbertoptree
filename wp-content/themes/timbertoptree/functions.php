<?php

function theme_includes() {
    wp_enqueue_style( 'ttt-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'theme_includes' );

function theme_setup() {
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_setup' );