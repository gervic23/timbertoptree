<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-US" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
   	<title><?php the_title(); ?> - <?php bloginfo('name'); ?></title>
    <link href='http://fonts.googleapis.com/css?family=Shojumaru' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Karla' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
</head>
<body>
    <div class="mainContainer">
        <header class="mainHeader">
			<img alt="logo" src="<?= get_template_directory_uri() ?>/img/timbertop_logo.png" />
			<div class="expertz">Tree Removal experts</div>
			<div class="headerNum">
				(585) 546-Tree (8733)
			</div>
		</header>
		<nav class="navMenu">
			<a href="<?= get_site_url() ?>">Home</a> | <a href="<?= get_site_url() ?>/contact-us/">Contact Us</a>
		</nav>