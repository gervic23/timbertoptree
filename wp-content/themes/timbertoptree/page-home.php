<?php get_header(); ?>

<div class="slideshow">
    <ul>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim1.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim2.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim3.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim4.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim5.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim6.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim7.jpg" /></li>
        <li><img src="<?= get_template_directory_uri() ?>/img/timber_slider/tim8.jpg" /></li>
    </ul>
    <div class="slideshowCaption">
        &nbsp;
    </div>
    <div class="slideLeft">&lt;</div>
    <div class="slideRight">&gt;</div>
</div>
<!-- End WP Header -->
<!-- Start WP Content -->
<section class="mainSection">
    <div class="wrapperContainer"></div>
    <div class="wrapperContents">
        <div class="tree_header"><img alt="tree_header" src="<?= get_template_directory_uri() ?>/img/services.png" /></div>
        <div class="uls">
            <ul>
                <li>Tree Trimming</li>
                <li>Tree Removal</li>
                <li>Stump Grinding</li>
                <li>Lot Clearing</li>
                <li>Landscape Renovation</li>
                <li>Snow Removal</li>
            </ul>
        </div>
    </div>
    <script>
        var $ = jQuery;
        
        $(document).ready(function() {
            window.slideshow_current_width = $(".slideshow").width();
            window.slideshow_current_height = $(".slideshow").height();
            window.maxlen = $(".slideshow ul li img").length * slideshow_current_width + 1;
            window.index = 1;
            window.t = setTimeout(imageSlider, 10000);

            for (var i = 0; i < $(".slideshow ul li img").length; i++) {
                $(".slideshow ul li img:nth-child(" + i + ")").css("width", slideshow_current_width);
                $(".slideshow ul li img:nth-child(" + i + ")").css("height", slideshow_current_height);
            }

            $(".slideshowCaption").css({"width": slideshow_current_width - 20});

            $(".slideRight").css({"margin-left": slideshow_current_width - 42});

            $(window).resize(function() {
                clearTimeout(t);

                index = 1;

                window.slideshow_current_width = $(".slideshow").width() + 7;
                window.slideshow_current_height = $(".slideshow").height();
                window.maxlen = $(".slideshow ul li img").length * slideshow_current_width;

                for (var i = 0; i < $(".slideshow ul li img").length; i++) {
                    $(".slideshow ul li img:nth-child(" + i + ")").css("width", slideshow_current_width);
                    $(".slideshow ul li img:nth-child(" + i + ")").css("height", slideshow_current_height);
                }

                $(".slideshowCaption").css({"width": slideshow_current_width - 20});

                $(".slideRight").css({"margin-left": slideshow_current_width - 42});

                window.t = setTimeout(imageSlider, 10000);
            });


            defaultState();

            $(".slideLeft").on("click", function() {
                clearTimeout(t);
                $(".slideLeft, .slideRight").hide();

                if (index <= $(".slideshow ul li").length && index !== 1) {
                    $(".slideshow ul li:nth-child(" + index + ")").fadeOut(1000, function() {
                        index--;
                        $(".slideshow ul li:nth-child( " + index + " )").fadeIn(500, function() {
                            $(".slideLeft, .slideRight").show();
                            imageCaption(index);
                        });
                    });
                } else if (index == 1) {
                    $(".slideshow ul li:nth-child(" + index + ")").fadeOut(1000, function() {
                        index = $(".slideshow ul li").length;
                        $(".slideshow ul li:nth-child( " + index + " )").fadeIn(500, function() {
                            $(".slideLeft, .slideRight").show();
                            imageCaption(index);
                        });
                    });
                }

                t = setTimeout(imageSlider, 7000);
            });

            $(".slideRight").on("click", function() {
                clearTimeout(t);
                $(".slideLeft, .slideRight").hide();

                if (index < $(".slideshow ul li").length) {
                    $(".slideshow ul li:nth-child(" + index + ")").fadeOut(1000, function() {
                        index++;
                        $(".slideshow ul li:nth-child( " + index + " )").fadeIn(500, function() {
                            $(".slideLeft, .slideRight").show();
                            imageCaption(index);
                        });
                    });
                } else if (index == $(".slideshow ul li").length) {
                    $(".slideshow ul li:nth-child(" + index + ")").fadeOut(1000, function() {
                        index = 1;
                        $(".slideshow ul li:nth-child(" + index + ")").fadeIn(500, function() {
                            $(".slideLeft, .slideRight").show();
                            imageCaption(index);
                        });
                    });
                }

                t = setTimeout(imageSlider, 7000);
            });

            function defaultState() {
                index = 1;
                $(".slideshow ul li:nth-child(1)").fadeIn(500, function() {
                    imageCaption(index);
                });
            }

            function imageSlider() {
                clearTimeout(t);
                $(".slideLeft, .slideRight").hide();

                if (index < $(".slideshow ul li").length) {
                    $(".slideshow ul li:nth-child(" + index + ")").fadeOut(1000, function() {
                        index++;
                        $(".slideshow ul li:nth-child( " + index + " )").fadeIn(500, function() {
                            $(".slideLeft, .slideRight").show();
                            imageCaption(index);
                        });
                    });
                } else if (index == $(".slideshow ul li").length) {
                    $(".slideshow ul li:nth-child(" + index + ")").fadeOut(1000, function() {
                        index = 1;
                        $(".slideshow ul li:nth-child(" + index + ")").fadeIn(500, function() {
                            $(".slideLeft, .slideRight").show();
                            imageCaption(index);
                        });
                    });
                }

                t = setTimeout(imageSlider, 7000);
            }

            function imageCaption(indx) {
                switch (indx) {
                    case 1: 
                        animateCaption("Snow Removal");
                    break;

                    case 2: 
                        animateCaption("Tree Trimming");
                    break;

                    case 3: 
                        animateCaption("Limbing Up");
                    break;

                    case 4: 
                        animateCaption("Complete Tree Removal");
                    break;

                    case 5: 
                        animateCaption("Canopy Reduction and Thinning");
                    break;

                    case 6: 
                        animateCaption("Storm Cleanup");
                    break;

                    case 7: 
                        animateCaption("Stump Grinding");
                    break;

                    case 8: 
                        animateCaption("Lot Clearing");
                    break;
                }
            }

            function animateCaption(str) {
                var c = 0;
                var len = str.length;

                var p = setInterval(function() {
                    c++;

                    if (c !== len) {
                        var s = str.substring(0, c);
                        $(".slideshowCaption").html(s + "_");
                    } else {
                        clearInterval(p);
                        $(".slideshowCaption").html(str);
                    }
                }, 30);
            }
        });
    </script>
</section>

<?php get_footer(); ?>