<?php get_header(); ?>

<section class="mainSection">
<div class="contactsParent">
    <div class="contactLinks">
        <span class="conInfo">Contact Info</span><span class="conMap">Google Map</span>
    </div>
    <div class="contactsWrapper"></div>
    <div class="contactsContents2">
        <div class="contactInfo">
            <p>
            Brad Hollowell<br />
            (585) 546-TREE<br />
        </p>
        <p>
            P.O Box 818<br />
            Pittsford, NY 14534
        </p>
        </div>
        <div class="contactMap">
            <iframe
              width="1030"
              height="290"
              frameborder="0" style="border:0"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23343.735115390235!2d-77.52191546548319!3d43.00003175207982!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d136e52c87b6db%3A0x426f0f6f2478a909!2sMendon%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1452276023382" allowfullscreen>
            </iframe>
        </div>
    </div>
</div>
    
<script type="text/javascript">
    var $ = jQuery;
    
    $(".conMap").on("click", function() {
        $(".contactInfo").fadeOut(500, function() {
            $(".contactMap").fadeIn(500, function() {
                $(".conMap").css("color", "#ea2831");
                $(".conInfo").css("color", "#fff");
            });
        });
    });

    $(".conInfo").on("click", function() {
        $(".contactMap").fadeOut(500, function() {
            $(".contactInfo").fadeIn(500, function() {
                $(".conInfo").css("color", "#ea2831");
                $(".conMap").css("color", "#fff");
            });
        });
    });
</script>

<?php get_footer(); ?>