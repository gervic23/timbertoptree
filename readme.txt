TimberTopTree Is a lumber company from New York USA. This is my 1st development project from freelancing. Powered by WordPress, and jQuery.

This source code is not intended to publish online. Please do not upload this source code to a webhost, or share to anyone. I only use this as a part of my portfolio.

======== INSTALLATION ========

Clone this repository to your web directory (htdocs, public_html).

== FOR WINDOWS XAMPP ==

* Copy and pase this tag to C:\xampp\apache\conf\extra\httpd-vhosts.conf

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "C:/xampp/htdocs/timbertoptree"
    ServerName timbertoptree.dev
    ServerAlias www.timbertoptree.dev
</VirtualHost>

* Add this code to C:\WINDOWS\SYSTEM32\Drivers\etc\hosts

127.0.01	timbertoptree.dev

* Create a database "timbertoptree".
* Import timbertoptree.sql to the database.
* Click wp_options table and look for option_name: site_url and home. Change option value to http://timbertoptree.dev
* Open wp-config.php and change database credentials.
* Restart apache.

== For Linux LAMP ==

* Run this command to your command terminal...

sudo gedit /etc/apache2/sites-available/000-default.conf

* Copy and paste this tag...

<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "/var/www/timbertoptree"
    ServerName timbertoptree.dev
    ServerAlias www.timbertoptree.dev
</VirtualHost>

* Run this command to your command terminal...

sudo gedit /etc/hosts

* Add this code to your hosts file.

127.0.01	timbertoptree.dev

* Create a database "timbertoptree".
* Import timbertoptree.sql to the database.
* Click wp_options table and look for option_name: site_url and home. Change option value to http://timbertoptree.dev
* Open wp-config.php and change database credentials.
* Restart apache by this command.

sudo systemctl restart apache2


== SYSTEM REQUIREMENTS ==
* PHP 5.4 or higher

== Admin Priviledge ==

http://timbertoptree.dev/wp-admin/

Username: admin
Password: onegervic23



